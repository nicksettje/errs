from errs import errs
import logging


@errs
def raises() -> int:
    raise Exception()
    return 0  # pragma: no cover


def check_error() -> None:
    out, err = raises()
    print('Error: {err}'.format(err=err.check()))


if __name__ == '__main__':
    logging.basicConfig(level=logging.ERROR, filename="log.log")
    logger = logging.getLogger(__name__)
    print(logger.__dict__)
    check_error()  # prints Error: True
