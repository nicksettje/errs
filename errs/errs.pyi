from typing import Any, Callable

class ErrorResult:
    is_error: bool = ...
    def __init__(self) -> None: ...
    def check(self) -> bool: ...

class NoError(ErrorResult):
    is_error: bool = ...
    def __init__(self) -> None: ...

class Error(ErrorResult):
    is_error: bool = ...
    traceback: Any = ...
    error: Any = ...
    def __init__(self, error: Exception) -> None: ...

def errs(func: Callable[..., Any]) -> Callable[..., Any]: ...
