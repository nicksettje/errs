# -*- coding: utf-8 -*-

__author__ = """Nick Frederick Settje"""
__email__ = 'nsettje@alumni.stanford.edu'
__version__ = '0.1.1'

from errs.errs import errs, ErrorResult  # noqa: F401
