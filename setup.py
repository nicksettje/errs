#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

with open('README.rst') as readme_file:
    readme = readme_file.read()

with open('requirements.txt', 'r') as f:
    extras_require = {
        'test': f.readlines()}

setup(
    author="Nick Frederick Settje",
    author_email='nsettje@alumni.stanford.edu',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        'Programming Language :: Python :: 3.7',
    ],
    description="Type-safe error handling for Python.",
    install_requires=[],
    license="MIT license",
    long_description=readme + '\n',
    include_package_data=True,
    package_data={'errs': ['py.typed']},
    keywords='errs',
    name='errs',
    packages=find_packages(include=['errs']),
    test_suite='tests',
    extras_require=extras_require,
    url='https://gitlab.com/nicksettje/errs',
    version='0.1.1',
    zip_safe=False,
)
